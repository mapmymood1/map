//
//  EmoticonCollViewController.swift
//  mapmymood
//
//  Created by Valentina Di Biase on 03/03/2017.
//  Copyright © 2017 IOSFOUNDATION. All rights reserved.
//

import UIKit

class EmoticonCollViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var userEmoticon :[String] = []
    var emoticon : [String] = ["entusiata","happy","intellettuale","malinconico","rilassato","romantico",]
    
    var infoToPass: UIImage?
    var userImage :[UIImage] = []
  
    
    
    @IBOutlet weak var emoticonColl: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ReadEmoticon()

        // Do any additional setup after loading the view.
    }

    //%%%%%%%%% COLLECTION
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return emoticon.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! EmoticonCell
        cell.emoticonImage.image = UIImage(named:emoticon[indexPath.row])
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        print("AAAAAAAAAAAAAAAA")
        
        userEmoticon.append(emoticon[indexPath.row])
        saveEmoticon(valuesToSetInMemory: userEmoticon)
        
        performSegue(withIdentifier: "backtoprofile", sender: nil)
    }

    
    //%%%%%%% SAVING
    
    
    func ReadEmoticon (){
        print("Loading Emoticon List from \(ItemArchiveURLEmoticon().path)")
        
        
        if let data = NSKeyedUnarchiver.unarchiveObject(withFile: ItemArchiveURLEmoticon().path) as? [String] {
            let valueInMemory = data
            print ("This is the number of saved Emoticon: \(valueInMemory.count) and this are the items: ")
            userEmoticon = valueInMemory
            for item in valueInMemory{
                print (item)
            }
        }
        return
    }
    
    func saveEmoticon(valuesToSetInMemory: [String]) {
        UserDefaults.standard.set(true, forKey: "isDataAlreadySaved")
        NSKeyedArchiver.archiveRootObject(valuesToSetInMemory, toFile: self.ItemArchiveURLEmoticon().path)
        
        
        print("Emoticon saved to the file")
    }
    
    
    
    func ItemArchiveURLEmoticon() -> URL {
        let documentPaths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return documentPaths[0].appendingPathComponent("faccina.plist")
    }
    

    
    
}
