//
//  touchID.swift
//  mapmymood
//
//  Created by Valentina Di Biase on 28/02/2017.
//  Copyright © 2017 IOSFOUNDATION. All rights reserved.
//

//
//  ViewController.swift
//  touch id
//
//  Created by Valentina Di Biase on 25/02/2017.
//  Copyright © 2017 IOSFOUNDATION. All rights reserved.
//

import UIKit
import LocalAuthentication

class Touchid: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authenticatewithTouchID()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func authenticatewithTouchID(){
        let localAuthContext = LAContext()
        let reasonText = "Authentication is required to sign in the application"
        var authError: NSError?
        
        if !localAuthContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError){
            if let error = authError {
                print(error.localizedDescription)
            }
            return
        }
        
        localAuthContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonText, reply: {(success:  Bool, error: Error?) -> Void in
            
            if !success {
                if let error = error {
                    switch error {
                    case LAError.authenticationFailed:
                        print("authentication failed")
                    case LAError.passcodeNotSet:
                        print("passcode not set")
                    case LAError.systemCancel:
                        print("authentication was canceled by system")
                    case LAError.userCancel:
                        print("authentication was canceled by the user")
                    case LAError.touchIDNotEnrolled:
                        print("authentication could not start because touch id has no enrolled fingers")
                    case LAError.touchIDNotAvailable:
                        print("authentication could not start because touch id is not available")
                    case LAError.userFallback:
                        print("user tapped the fallback button(enter Login).")
                        
                    default:
                        print(error.localizedDescription)
                    }
                }
                OperationQueue.main.addOperation ({
                    print("FALLBACK")
                    self.performSegue(withIdentifier: "showLogin", sender: nil)
                })
            }
            else{
                print("successfully authenticated")
                OperationQueue.main.addOperation ({
                    
                    self.performSegue(withIdentifier: "authenticated", sender: nil)
                })
            }
        })
        
    }
    
}
