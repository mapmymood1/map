//
//  UserController.swift
//  mapmymood
//
//  Created by Valentina Di Biase on 03/03/2017.
//  Copyright © 2017 IOSFOUNDATION. All rights reserved.
//

import UIKit

class UserController: UIViewController, UICollectionViewDataSource {
    
    var userEmoticon : [String] = []
    
    var usersPhotos : [UIImage] = []
    
    

    @IBOutlet weak var photoCollection: UICollectionView!
    @IBOutlet weak var profileImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // usersPhotos.append(infoPassed!)
        ReadImage()
        ReadEmoticon()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userEmoticon.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCell
        cell.backgroundView?.backgroundColor = UIColor.red
        cell.image.image = usersPhotos[indexPath.row]
        cell.emoticonImage.image = UIImage(named: userEmoticon[indexPath.row])
  
        return cell
    }

    
    
    func ReadImage (){
        print("Loading Item List from \(ItemArchiveURL().path)")
        
       
        if let data = NSKeyedUnarchiver.unarchiveObject(withFile: ItemArchiveURL().path) as? [UIImage] {
            let valueInMemory = data
            print ("This is the number of saved item: \(valueInMemory.count) and this are the items: ")
            usersPhotos = valueInMemory
            for item in valueInMemory{
                print (item)
            }
        }
        return
    }
    
    func saveImage(valuesToSetInMemory: [UIImage]) {
        UserDefaults.standard.set(true, forKey: "isDataAlreadySaved")
        NSKeyedArchiver.archiveRootObject(valuesToSetInMemory, toFile: self.ItemArchiveURL().path)
        
        
        print("Items saved to the file")
    }
    
    func ItemArchiveURL() -> URL {
        let documentPaths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return documentPaths[0].appendingPathComponent("fotina.plist")
    }
    
    
    
    
    
    func ReadEmoticon (){
        print("Loading Emoticon List from \(ItemArchiveURLEmoticon().path)")
        
        
        if let data = NSKeyedUnarchiver.unarchiveObject(withFile: ItemArchiveURLEmoticon().path) as? [String] {
            let valueInMemory = data
            print ("This is the number of saved Emoticon: \(valueInMemory.count) and this are the items: ")
            userEmoticon = valueInMemory
            for item in valueInMemory{
                print (item)
            }
        }
        return
    }
    
    func saveEmoticon(valuesToSetInMemory: [String]) {
        UserDefaults.standard.set(true, forKey: "isDataAlreadySaved")
        NSKeyedArchiver.archiveRootObject(valuesToSetInMemory, toFile: self.ItemArchiveURLEmoticon().path)
        
        
        print("Emoticon saved to the file")
    }
    
    
    
    func ItemArchiveURLEmoticon() -> URL {
        let documentPaths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return documentPaths[0].appendingPathComponent("faccina.plist")
    }
    
    
    
    

}
