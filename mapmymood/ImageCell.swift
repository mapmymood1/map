//
//  ImageCell.swift
//  mapmymood
//
//  Created by Valentina Di Biase on 03/03/2017.
//  Copyright © 2017 IOSFOUNDATION. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var emoticonImage: UIImageView!
    @IBOutlet weak var image: UIImageView!
}
