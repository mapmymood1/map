//
//  EmoticonCell.swift
//  mapmymood
//
//  Created by Valentina Di Biase on 03/03/2017.
//  Copyright © 2017 IOSFOUNDATION. All rights reserved.
//

import UIKit

class EmoticonCell: UICollectionViewCell {
    
    @IBOutlet weak var emoticonImage: UIImageView!
}
