//
//  Camera.swift
//  mapmymood
//
//  Created by Valentina Di Biase on 27/02/2017.
//  Copyright © 2017 IOSFOUNDATION. All rights reserved.
//

import Foundation
import UIKit


class CameraController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   
  

    var infoToPass: UIImage?
    var userImage :[UIImage] = []
    var  userEmoticon  :[UIImage] = []
     let imagePickerController = UIImagePickerController()

   
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ReadValues()
       
        
        imagePickerController.delegate = self
        
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePickerController.sourceType = .camera
                self.present(self.imagePickerController, animated: true, completion: nil)
            }else{
                print("Camera not available")
            }
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
           
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        userImage.append(image)
        saveImage(valuesToSetInMemory: userImage)
        
        
        imageView.image = image
       
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);

        
       self.dismiss(animated: true) { 
        self.performSegue(withIdentifier: "avanti", sender: nil)
        }
    }
    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        
//        if segue.identifier == "showprofile"{
//        let destinationController = segue.destination as? UserController
//           // destinationController?.infoPassed = infoToPass
//        
//        }
//        
//    }
    
    
    
    func ReadValues (){
        print("Loading Item List from \(ItemArchiveURL().path)")
        if let data = NSKeyedUnarchiver.unarchiveObject(withFile: ItemArchiveURL().path) as? [UIImage] {
            let valueInMemory = data
            print ("This is the number of saved item: \(valueInMemory.count) and this are the items: ")
            userImage = valueInMemory
            for item in valueInMemory{
                print (item)
            }
        }
        
    }
    
    func saveImage(valuesToSetInMemory: [UIImage]) {
        UserDefaults.standard.set(true, forKey: "isDataAlreadySaved")
        NSKeyedArchiver.archiveRootObject(valuesToSetInMemory, toFile: self.ItemArchiveURL().path)
        
        print("Items saved to the file")
    }
    
    func ItemArchiveURL() -> URL {
        let documentPaths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return documentPaths[0].appendingPathComponent("fotina.plist")
    }
    
    

    
    


    
}
